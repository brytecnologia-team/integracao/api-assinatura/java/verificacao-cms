package br.com.bry.framework.verificacao.main;

import br.com.bry.framework.verificacao.VerifierCMS;

public class Application {

	public static void main(String[] args) {

		VerifierCMS cmsVerification = new VerifierCMS();

		System.out.println(
				"=====================================Verification of attached signature:=====================================");

		cmsVerification.verifyCAdESattachedSignature();

		System.out.println();

		System.out.println(
				"=====================================Verification of detached signature:====================================");

		cmsVerification.verifyCAdESdetachedSignature();

		System.out.println();

		System.out.println(
				"===================Verification of detached signature by sending hash of original document:==================");

		cmsVerification.verifyCMSdetachedSignatureBySendingHashOfOriginalDocument();

	}

}
