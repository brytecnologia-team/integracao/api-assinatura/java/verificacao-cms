package br.com.bry.framework.verificacao;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bry.framework.verificacao.config.ServiceConfig;
import br.com.bry.framework.verificacao.config.VerifierConfig;
import br.com.bry.framework.verificacao.util.ConverterUtil;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class VerifierCMS {

	private String PATH_SEPARATOR = File.separator;

	private String token = ServiceConfig.ACCESS_TOKEN;

	private static final String URL_SERVER_CMS = ServiceConfig.URL_CMS_VERIFIER;

	private ConverterUtil converterUtil = new ConverterUtil();

	public void verifyCAdESattachedSignature() {

		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			System.out.println("Configure um token v�lido!");
			return;
		}

		try {
			Object verifierResponse = null;
			String filePath = VerifierConfig.ATTACHED_SIGNATURE_PATH;

			RequestSpecBuilder builder = new RequestSpecBuilder();

			int signatureQuantitiesToVerify = 1;

			for (int indexOfSignature = 0; indexOfSignature < signatureQuantitiesToVerify; indexOfSignature++) {
				builder.addMultiPart("signatures[" + indexOfSignature + "][nonce]", Integer.toString(indexOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][content]", new File(filePath));
			}

			RequestSpecification requestSpec = builder.build();

			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec)
					.multiPart("nonce", VerifierConfig.NONCE)
					.multiPart("contentsReturn", VerifierConfig.CONTENTS_RETURN).expect().statusCode(200).when()
					.post(URL_SERVER_CMS).as(Object.class);

			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println(jsonResponse);

			JSONObject jsonObject = new JSONObject(jsonResponse);

			JSONArray verificationStatus = jsonObject.getJSONArray("verificationStatus");

			String verificationStatusStringJson = verificationStatus.getString(0);

			JSONObject verificationStatusjsonObject = new JSONObject(verificationStatusStringJson);

			int nonceOfverificationStatus = verificationStatusjsonObject.getInt("nonce");

			String content = verificationStatusjsonObject.getString("generalStatus");

			System.out.println("Nonce of verified attached signature: " + nonceOfverificationStatus);

			System.out.println("Verification Status : " + content);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void verifyCAdESdetachedSignature() {
		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			System.out.println("Configure um token v�lido!");
			return;
		}

		try {
			Object verifierResponse = null;
			String filePathOfSignature = VerifierConfig.DETACHED_SIGNATURE_PATH;
			System.out.println("Caminho da assinatura: " + filePathOfSignature);

			String filePathOfOriginalDocument = VerifierConfig.ORIGINAL_DOCUMENT_PATH;

			RequestSpecBuilder builder = new RequestSpecBuilder();

			int signatureQuantitiesToVerify = 1;

			for (int indexOfSignature = 0; indexOfSignature < signatureQuantitiesToVerify; indexOfSignature++) {
				builder.addMultiPart("signatures[" + indexOfSignature + "][nonce]", Integer.toString(indexOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][content]", new File(filePathOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][documentContent]",
						new File(filePathOfOriginalDocument));
			}

			RequestSpecification requestSpec = builder.build();

			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec)
					.multiPart("nonce", VerifierConfig.NONCE)
					.multiPart("contentsReturn", VerifierConfig.CONTENTS_RETURN).expect().statusCode(200).when()
					.post(URL_SERVER_CMS).as(Object.class);

			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println(jsonResponse);

			JSONObject jsonObject = new JSONObject(jsonResponse);

			JSONArray verificationStatus = jsonObject.getJSONArray("verificationStatus");

			String verificationStatusStringJson = verificationStatus.getString(0);

			JSONObject verificationStatusjsonObject = new JSONObject(verificationStatusStringJson);

			int nonceOfverificationStatus = verificationStatusjsonObject.getInt("nonce");

			String content = verificationStatusjsonObject.getString("generalStatus");

			System.out.println("Nonce of verified detached signature: " + nonceOfverificationStatus);

			System.out.println("Verification status: " + content);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void verifyCMSdetachedSignatureBySendingHashOfOriginalDocument() {
		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			System.out.println("Configure um token v�lido!");
			return;
		}

		try {
			Object verifierResponse = null;
			String filePathOfSignature = VerifierConfig.DETACHED_SIGNATURE_PATH;

			String originalDocumentHash = VerifierConfig.HASH_OF_ORIGINAL_DOCUMENT;

			RequestSpecBuilder builder = new RequestSpecBuilder();

			int signatureQuantitiesToVerify = 1;

			for (int indexOfSignature = 0; indexOfSignature < signatureQuantitiesToVerify; indexOfSignature++) {
				builder.addMultiPart("signatures[" + indexOfSignature + "][nonce]", Integer.toString(indexOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][content]", new File(filePathOfSignature));
				builder.addMultiPart("signatures[" + indexOfSignature + "][documentHashes][0]", originalDocumentHash);
			}

			RequestSpecification requestSpec = builder.build();

			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec)
					.multiPart("nonce", VerifierConfig.NONCE)
					.multiPart("contentsReturn", VerifierConfig.CONTENTS_RETURN).expect().statusCode(200).when()
					.post(URL_SERVER_CMS).as(Object.class);

			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println(jsonResponse);

			JSONObject jsonObject = new JSONObject(jsonResponse);

			JSONArray verificationStatus = jsonObject.getJSONArray("verificationStatus");

			String verificationStatusStringJson = verificationStatus.getString(0);

			JSONObject verificationStatusjsonObject = new JSONObject(verificationStatusStringJson);

			int nonceOfverificationStatus = verificationStatusjsonObject.getInt("nonce");

			String content = verificationStatusjsonObject.getString("generalStatus");

			System.out.println("Nonce of verified detached signature: " + nonceOfverificationStatus);

			System.out.println("Verification status : " + content);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
