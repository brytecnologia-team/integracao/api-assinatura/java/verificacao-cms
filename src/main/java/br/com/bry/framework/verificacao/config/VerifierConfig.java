package br.com.bry.framework.verificacao.config;

import java.io.File;

public class VerifierConfig {

	private static final String PATH_SEPARATOR = File.separator;

	// Request identifier
	public static final String NONCE = "1";

	// Available values: 'true' = Report with signature timestamp information and
	// 'false' = Report without signature timestamp information
	public static final boolean CONTENTS_RETURN = true;

	// location where the attached signature is stored
	public static final String ATTACHED_SIGNATURE_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "assinaturas" + PATH_SEPARATOR
			+ "attached" + PATH_SEPARATOR + "AssinaturaAttachedADRC.p7s";

	// location where the detached signature is stored
	public static final String DETACHED_SIGNATURE_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "assinaturas" + PATH_SEPARATOR
			+ "detached" + PATH_SEPARATOR + "assinaturaDetached.p7s";

	// location where the original document is stored
	public static final String ORIGINAL_DOCUMENT_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "assinaturas" + PATH_SEPARATOR
			+ "detached" + PATH_SEPARATOR + "exemploTagAccept.pdf";

	public static final String HASH_OF_ORIGINAL_DOCUMENT = "05E6F73A05385C4A9E96B15EF3C75A75DE91632EE7DEB63BE098BC61072898FB";

}
